% Inspector prolog
% Joao Costa DaQuinta, Lea Heiniger

% Global fact database 

% This file is the global template 
% used to define cases

:- use_module(library(clpfd)).

%%%%%%%%%% Case Definition %%%%%%%%%%

% case_name(String). : The title of the case

% case_introduction(String). : As small text to 
% introduce the case to the player


% person(P). : The persons involved in the case

%Exemple
%person(lea).
%person(joao).

% victim(P). : the person murdered

%Exemple
%victim(joao).


% object(O). : The objects in the case 
% (clues, clothes of the suspects, ...)

%Exemple
%object(hair_clip).
%object(jeans).

% cigarette_butt(O). : Usefull to link smokers to 
% the crime scene

%Exemple
%cigarette_butt(cigarette__1).

% has_color(O, String) : The color of an object

%Exemple
%has_color(hair_clip, 'green').

% has_detail(O, String). : a message about an object 
% that the player can see 

%Exemple
%has_detail(hair_clip, 'This hair clip is broken.').


% location(L). : The locations where the payer can go

%Exemple
%location(room_318).
%location(corridor).

% crime_scene(L). : The location where the 
% crime took place

%Exemple
%crime_scene(corridor).


% pretender_location(P, L). : The location where a 
% suspect says they were at the time of the crime

%Exemple
%pretended_location(lea, room_318).

% saw(P1, P2). : When a suspect says they saw 
% another person

%Exemple
%saw(lea, joao).

% wears(P, O). : The clothes and objects on the suspects

%Exemple
%wears(lea, jeans).

% smoker(P). : The persons that smokes 
% (thex can live a cigarette butt in a location)

%Exemple
%smoker(lea).

% lost(P, O). : The objects lost by the suspects

%Exemple
%lost(lea, hair_clip).


% found_in(O, L). : The objects (clues) found_in 
% each location

%Exemple
%found_in(hair_clip, corridor).


% number of proofs required to prosecute a suspect
% The default value is 0, but it can change 
% depending of the case

required_nbr_proofs(N) :- N>=0.

%%%%%%%%%% Logical Rules %%%%%%%%%%

%%%       Finding the murderer       %%%

% supect(P).

% all the persons (except the victim) who hav not 
% been proved not guilty yet
suspect(P):- person(P), \+victim(P), \+not_guilty(P).


% not_guilty(P).

% all the persons with an alibi
not_guilty(P) :- has_alibi(P).


% murderer(P).

% if someone is the only suspect left
% they are the murderer
murderer(P) :- forall(suspect(S), S=P).


% was_in(P, L).

% a person can be in only one location
was_in(P, L) :- person(P), forall(location(X), X=L). 

% someone who lost an object in 
% a location was in this location
was_in(P, L) :- lost(P, O), found_in(O, L).

% if someone was seen by a person who tells 
% the truth, they were in the same location
was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L).


% liar(P). and tells_the_truth(P).

% if someone was not where they pretended 
% to be the persone lies
tells_the_truth(P) :- pretended_location(P, L), was_in(P, L).

% if someone is not telling the truth
% they lie
liar(P) :- \+tells_the_truth(P).


% has_alibi(P)

% someone who was in a location different 
% from the crime scene has an alibi
has_alibi(P) :- was_in(P, L), \+crime_scene(L).


%%%          Proofs system         %%%

% is_proof_against(O, P).

% an object lost by someone on the 
% crime scene is a proof against them
is_proof_against(O, P) :- lost(P, O), found_in(O, L), crime_scene(L).

% if the victim doesn't smoke, a cigarette butt on the 
% crime scene is a proof against smoking suspects
is_proof_against(M, P) :- cigarette_butt(M), found_in(M, L), crime_scene(L), smoker(P), victim(V), \+smoker(V).


% list_of_proof(L). : Whe verify that every element 
% of the list is a proof against the murderer

list_of_proofs([]).
list_of_proofs([H|L]) :- is_proof_against(H, P), murderer(P), list_of_proofs(L).


% number_of_proofs(L, N). : Length of a list of proofs

number_of_proofs([], 0).
number_of_proofs([_|L], Z) :- list_of_proofs(L), number_of_proofs(L, Y), Z #= Y+1.


% can_be_prosecuted(P, L).

% if we have at least the minimum number of proofs 
% required against the murderer they can be prosecuted
can_be_prosecuted(P, L) :- murderer(P), list_of_proofs(L), number_of_proofs(L,N), required_nbr_proofs(N).