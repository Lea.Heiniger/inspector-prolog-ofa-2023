% Inspector prolog
% Joao Costa DaQuinta, Lea Heiniger

% Scenario 1

% This file implements the scenario 
% described in the file scenario1.md 

:- use_module(library(clpfd)).

%%%%%%%%%% Case Definition %%%%%%%%%%

% case_name(String). : The title of the case
case_name('The murder  of Mr X').

% case_introduction(String). : As small text to 
% introduce the case to the player
case_introduction('Mr X is found stabbed to death in his bedroom. His house is very well secured. The murderer must therefore have been one of the people there that evening. You have to find out who it is and at least one piece of evidence to convict them.').


% person(P). : The persons involved in the case
person(mister_X).
person(jean_thy).
person(amy_khale).
person(sam_patick).
person(chris_minel).

% victim(P). : the person murdered
victim(mister_X).


% object(O). : The objects in the case 
% (clues, clothes of the suspects, ...)
object(hat).
object(earring__g).
object(knife).
object(watch).
object(earring__s).
object(handkerchief).
object(shopping_list).
object(glasses).

object(coat__j).
object(trousers__j).
object(glasses__a).
object(dress__a).
object(earring__a).
object(suit__p).
object(cap__c).
object(jumper__c).
object(jeans__c).

% cigarette_butt(O). : Usefull to link smokers to 
% the crime scene
cigarette_butt(cigarette__1).
cigarette_butt(cigarette__2).

% has_color(O, String) : The color of an object
has_color(hat, 'green').
has_color(earring__g, 'gold').
has_color(watch, 'black').
has_color(handkerchief, 'blue').
has_color(earring__s, 'silver').

has_color(coat__j, 'black').
has_color(trousers__j, 'brown').
has_color(dress__a, 'blue').
has_color(earring__a, 'silver').
has_color(suit__p, 'green').
has_color(cap__c, 'black').
has_color(jumper__c, 'white').

% has_detail(O, String). : a message about an object 
% that the player can see 
has_detail(earring__s, 'It looks just like the earring Amy still wears.').
has_detail(knife, 'There is blood on this knif.').


% location(L). : The locations where the payer can go
location(bedroom).
location(kitchen).
location(living_room).
location(library).

% crime_scene(L). : The location where the 
% crime took place
crime_scene(bedroom).


% pretender_location(P, L). : The location where a 
% suspect says they were at the time of the crime
pretended_location(jean_thy, library).
pretended_location(amy_khale, living_room).
pretended_location(sam_patick, library).
pretended_location(chris_minel, kitchen).

% saw(P1, P2). : When a suspect says they saw 
% another person
saw(sam_patick, jean_thy). % to comment for liar() tests
saw(jean_thy, sam_patick).

% wears(P, O). : The clothes and objects on the suspects

% Jean Thy
wears(jean_thy,coat__j).
wears(jean_thy, trousers__j).

% Amy Khale
wears(amy_khale, dress__a).
wears(amy_khale, glasses__a).
wears(amy_khale, earring__a).

% Sam Patick
wears(sam_patick, suit__p).

% Chris Minel
wears(chris_minel, cap__c).
wears(chris_minel, jumper__c).
wears(chris_minel, jeans__c).

% smoker(P). : The persons that smokes 
% (thex can live a cigarette butt in a location)
smoker(amy_khale).
smoker(sam_patick).
smoker(chris_minel).

% lost(P, O). : The objects lost by the suspects
lost(amy_khale, earring__s).
lost(sam_patick, glasses).


% found_in(O, L). : The objects (clues) found_in 
% each location

% Bedroom 
found_in(cigarette__1, bedroom).
found_in(hat, bedroom).
found_in(earring__g, bedroom).

% Kitchen
found_in(knife, kitchen).
found_in(watch, kitchen).

% Living room
found_in(earring__s, living_room).
found_in(handkerchief, living_room).
found_in(cigarette__2, living_room).

% Library
found_in(shopping_list, library).
found_in(glasses, library).


% number of proofs required to prosecute 
% a suspect, here at least one
required_nbr_proofs(N) :- N>=1.

%%%%%%%%%% Logical Rules %%%%%%%%%%

%%%       Finding the murderer       %%%

% supect(P).

% all the persons (except the victim) who hav not 
% been proved not guilty yet
suspect(P):- person(P), \+victim(P), \+not_guilty(P).


% not_guilty(P).

% all the persons with an alibi
not_guilty(P) :- has_alibi(P).


% murderer(P).

% if someone is the only suspect left
% they are the murderer
murderer(P) :- forall(suspect(S), S=P).


% was_in(P, L).

% a person can be in only one location
was_in(P, L) :- person(P), forall(location(X), X=L). 

% someone who lost an object in 
% a location was in this location
was_in(P, L) :- lost(P, O), found_in(O, L).

% if someone was seen by a person who tells 
% the truth, they were in the same location
was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L).


% liar(P). and tells_the_truth(P).

% if someone was not where they pretended 
% to be the persone lies
tells_the_truth(P) :- pretended_location(P, L), was_in(P, L).

% if someone is not telling the truth
% they lie
liar(P) :- \+tells_the_truth(P).


% has_alibi(P)

% someone who was in a location different 
% from the crime scene has an alibi
has_alibi(P) :- was_in(P, L), \+crime_scene(L).


%%%          Proofs system         %%%

% is_proof_against(O, P).

% an object lost by someone on the 
% crime scene is a proof against them
is_proof_against(O, P) :- lost(P, O), found_in(O, L), crime_scene(L).

% if the victim doesn't smoke, a cigarette butt on the 
% crime scene is a proof against smoking suspects
is_proof_against(M, P) :- cigarette_butt(M), found_in(M, L), crime_scene(L), smoker(P), victim(V), \+smoker(V).


% list_of_proof(L). : Whe verify that every element 
% of the list is a proof against the murderer

list_of_proofs([]).
list_of_proofs([H|L]) :- is_proof_against(H, P), murderer(P), list_of_proofs(L).


% number_of_proofs(L, N). : Length of a list of proofs

number_of_proofs([], 0).
number_of_proofs([_|L], Z) :- list_of_proofs(L), number_of_proofs(L, Y), Z #= Y+1.


% can_be_prosecuted(P, L).

% if we have at least the minimum number of proofs 
% required against the murderer they can be prosecuted
can_be_prosecuted(P, L) :- murderer(P), list_of_proofs(L), number_of_proofs(L,N), required_nbr_proofs(N).