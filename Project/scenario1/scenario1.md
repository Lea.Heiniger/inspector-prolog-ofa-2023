# Scenario 1 : The murder  of Mr X  

Mr X is found stabbed to death in his **bedroom**. His house has three other rooms (the **living room**, the **kitchen** and the **library**) and is very well secured. The murderer must therefore have been one of the people there that evening.  
Once you have identified the murderer, you will also need to find at least one piece of evidence against them in order to take them to court.  

## The suspects:  

- **Jean Thy** : He wears a black coat and brown trousers. He says that at the time of the murder he was in the library with Sam.  
- **Amy Khale** : She smokes. She wears a single silver earring, glasses and a blue dress. She says that at the time of the crime she was in the living room.  
- **Sam Patick** : He smokes. He wears a green suit and glasses (which he has lost). He says that at the time of the crime he was in the library with Jean.  
- **Chris Minel** : He smokes. He wears a black cap, white jumper and jeans. he says that at the time of the crime he was in the kitchen.  

## Clues (by room) :  

- **Bedroom** : cigarette butt (non-smoking victim), green hat, gold earring.  
- **Kitchen** : knife (murder weapon), black watch.  
- **Living room** : silver earring, blue handkerchief.  
- **Library** : shopping list, glasses.