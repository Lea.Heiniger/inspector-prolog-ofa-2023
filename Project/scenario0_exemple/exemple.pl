% Inspector prolog
% Joao Costa DaQuinta, Lea Heiniger

% Exemple of case

% This file is the exemple of a small case 
% implemented (scenario in file exemple.md)

:- use_module(library(clpfd)).

%%%%%%%%%% Case Definition %%%%%%%%%%

% case_name(String). : The title of the case
case_name('An OFM project gone wrong').

% case_introduction(String). : As small text to 
% introduce the case to the player
case_introduction('Joao is found dead in one of Batelle s corridors. Someone had strangled him using an HDMI cable. You have to find out who did it by investigating the people present at the scene.').


% person(P). : The persons involved in the case
person(lea).
person(joao).
person(damien).
person(pr_buchs).
person(mysterious_student).

% victim(P). : the person murdered
victim(joao).


% object(O). : The objects in the case 
% (clues, clothes of the suspects, ...)
object(laptop).
object(hdmi_wire).
object(wallet).
object(hair_clip).

object(jeans__l).
object(sweatshirt__l).
object(jeans__d).
object(teeshirt__d).
object(trousers__pb).
object(shirt__pb).
object(shirt__ms).
object(jeans__ms).

% cigarette_butt(O). : Usefull to link smokers 
% to the crime scene
cigarette_butt(cigarette).

% has_color(O, String) : The color of an object
has_color(wallet, 'blue').
has_color(hair_clip, 'green').
has_color(sweatshirt__l, 'red').
has_color(teeshirt__d, 'grey').
has_color(shirt__pb, 'blue').
has_color(trousers__pb, 'beige').
has_color(shirt__ms, 'black').

% has_detail(O, String). : a message about an object 
% that the player can see 
has_detail(laptop, 'The username on the login screen is Damien.').


% location(L). : The locations where the payer can go
location(room_318).
location(corridor).
location(local_aei).

% crime_scene(L). : The location where the 
% crime took place
crime_scene(corridor).


% pretender_location(P, L). : The location where a 
% suspect says they were at the time of the crime
pretended_location(lea, local_aei).
pretended_location(damien, room_318).
pretended_location(pr_buchs, room_318).
pretended_location(mysterious_student, local_aei).

% saw(P1, P2). : When a suspect says they saw 
% another person
saw(damien, pr_buchs).
saw(pr_buchs, damien).

% wears(P, O). : The clothes and objects on the suspects

% Lea
wears(lea, jeans__l).
wears(lea, sweatshirt__l).

% Damien
wears(damien, jeans__d).
wears(damien, teeshirt__d).

% Pr Buchs
wears(pr_buchs, trousers__pb).
wears(pr_buchs, shirt__pb).

% Mysterious student
wears(mysterious_student, jeans__ms).
wears(mysterious_student, shirt__ms).

% smoker(P). : The persons that smokes 
% (thex can live a cigarette butt in a location)
smoker(mysterious_student).

% lost(P, O). : The objects lost by the suspects
lost(lea, hair_clip).
lost(damien, laptop).
lost(mysterious_student, wallet).


% found_in(O, L). : The objects (clues) found_in 
% each location

% Room 318
found_in(laptop, room_318).

% Corridor
found_in(hair_clip, corridor).
found_in(hdmi_wire, corridor).

% Local AEI
found_in(wallet, local_aei).
found_in(cigarette, local_aei).


% number of proofs required to prosecute 
% a suspect, here at least one
required_nbr_proofs(N) :- N>=1.

%%%%%%%%%% Logical Rules %%%%%%%%%%

%%%       Finding the murderer       %%%

% supect(P).

% all the persons (except the victim) who hav not 
% been proved not guilty yet
suspect(P):- person(P), \+victim(P), \+not_guilty(P).


% not_guilty(P).

% all the persons with an alibi
not_guilty(P) :- has_alibi(P).


% murderer(P).

% if someone is the only suspect left
% they are the murderer
murderer(P) :- forall(suspect(S), S=P).


% was_in(P, L).

% a person can be in only one location
was_in(P, L) :- person(P), forall(location(X), X=L). 

% someone who lost an object in 
% a location was in this location
was_in(P, L) :- lost(P, O), found_in(O, L).

% if someone was seen by a person who tells 
% the truth, they were in the same location
was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L).


% liar(P). and tells_the_truth(P).

% if someone was not where they pretended 
% to be the persone lies
tells_the_truth(P) :- pretended_location(P, L), was_in(P, L).

% if someone is not telling the truth
% they lie
liar(P) :- \+tells_the_truth(P).


% has_alibi(P)

% someone who was in a location different 
% from the crime scene has an alibi
has_alibi(P) :- was_in(P, L), \+crime_scene(L).


%%%          Proofs system         %%%

% is_proof_against(O, P).

% an object lost by someone on the 
% crime scene is a proof against them
is_proof_against(O, P) :- lost(P, O), found_in(O, L), crime_scene(L).

% if the victim doesn't smoke, a cigarette butt on the 
% crime scene is a proof against smoking suspects
is_proof_against(M, P) :- cigarette_butt(M), found_in(M, L), crime_scene(L), smoker(P), victim(V), \+smoker(V).


% list_of_proof(L). : Whe verify that every element 
% of the list is a proof against the murderer

list_of_proofs([]).
list_of_proofs([H|L]) :- is_proof_against(H, P), murderer(P), list_of_proofs(L).


% number_of_proofs(L, N). : Length of a list of proofs

number_of_proofs([], 0).
number_of_proofs([_|L], Z) :- list_of_proofs(L), number_of_proofs(L, Y), Z #= Y+1.


% can_be_prosecuted(P, L).

% if we have at least the minimum number of proofs 
% required against the murderer they can be prosecuted
can_be_prosecuted(P, L) :- murderer(P), list_of_proofs(L), number_of_proofs(L,N), required_nbr_proofs(N).