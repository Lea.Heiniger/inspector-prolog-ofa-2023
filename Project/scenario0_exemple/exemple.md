# Exemple case : An OFM project gone wrong  

Joao is found dead in one of Batelle's **corridors**. Someone had strangled him using an HDMI cable. The suspects are the peoples present in the rooms nearby (**room 318** and the **local AEI**).  

## The suspects:  

- **Lea** : She wears a jeans and a red sweatshirt. Usually she has her hair attached with a green hair clip but you can't see it. She says that she was in the Local AEI at the time of the murder.  
- **Damien** : He wears a jeans and a black t-shirt. He says that he was in room 318 with Pr Buchs.  
- **Pr Buchs** : He wears beige trousers and a blue shirt. He says that he was working with Damien in room 318.  
- **A mysterious student** : He smokes. He wears a jeans and a black shirt. He says that at the time of the murder he was in the Local AEI. He also tells you that he lost his wallet.  

## Clues (by room) :  

- **Corridor** : a green hair clip and an HDMI cable (the murder weapon)  
- **Room 318** : Damien's laptop.  
- **Local AEI** : a blue wallet.  
