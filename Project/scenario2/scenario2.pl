% Inspector prolog
% Joao Costa DaQuinta, Lea Heiniger

% Scenario 2

% This file implements the scenario 
% described in the file scenario2.md 

:- use_module(library(clpfd)).

%%%%%%%%%% Case Definition %%%%%%%%%%

% case_name(String). : The title of the case
case_name('Murder in the basement').

% case_introduction(String). : As small text to 
% introduce the case to the player
case_introduction('Dick Raicteur is found murdered in the basement of the small inn where he runs the business. Everyone present on the inn that evening is under suspicion. You have to find out who the murderer is and at least two pieces of evidence to convict Them.').


% person(P). : The persons involved in the case
person(dick_raicteur).
person(gordon_bleu).
person(sally_riez).
person(pierre_sonnel).
person(charles_hatant).
person(fab_ullateur).
person(klint_yhan).

% victim(P). : the person murdered
victim(dick_raicteur).


% object(O). : The objects in the case 
% (clues, clothes of the suspects, ...)
object(pistol).
object(bag_of_flour).
object(glasses).
object(necklace__s).
object(napkin).
object(chef_hat__gb).
object(apron).
object(newspaper).
object(handkerchief).
object(account_book).
object(mail).
object(suit).
object(ring).
object(watch).
object(tie).

object(cook_uniform__g).
object(trousers__s).
object(top__s).
object(trousers__p).
object(shirt__p).
object(trousers__c).
object(woollen_jumper__c).
object(jeans__f).
object(shirt__f).
object(pyjamas__k).

% cigarette_butt(O). : Usefull to link smokers 
% to the crime scene
cigarette_butt(cigarette__1).

% has_color(O, String) : The color of an object
has_color(necklace__s, 'silver').
has_color(napkin, 'beige').
has_color(apron, 'black').
has_color(handkerchief, 'blue').
has_color(suit, 'black').
has_color(ring, 'silver').
has_color(watch, 'brown').
has_color(tie, 'blue').
has_color(trousers__s, 'black').
has_color(top__s, 'black').
has_color(trousers__p, 'black').
has_color(shirt__p, 'black').
has_color(trousers__c, 'blue').
has_color(woollen_jumper__c, 'grey').
has_color(shirt__f, 'white').
has_color(pyjamas__k, 'green').

% has_detail(O, String). : a message about an object 
% that the player can see 
has_detail(necklace__s, 'The pendant is in the shape of an S.').
has_detail(chef_hat__gb, 'It has the initials G.B. on the front.').
has_detail(suit, 'The initials K.Y. are embroidered on the inside.').
has_detail(newspaper, 'No fresh news this is the paper from yesterday.').
has_detail(account_book, 'It contains the accounts of the inn.').
has_detail(mail, 'It is the mail for Dick, you did not find anything of interest in it.').


% location(L). : The locations where the payer can go
location(basement).
location(kitchen).
location(restaurant).
location(toilets).
location(boudoir).
location(dick_bedroom).
location(room_1).
location(room_2).

% crime_scene(L). : The location where the 
% crime took place
crime_scene(basement).


% pretender_location(P, L). : The location where a 
% suspect says they were at the time of the crime
pretended_location(gordon_bleu, kitchen).
pretended_location(sally_riez, restaurant).
pretended_location(pierre_sonnel, kitchen).
pretended_location(charles_hatant, boudoir).
pretended_location(fab_ullateur, boudoir).
pretended_location(klint_yhan, room_1).

% saw(P1, P2). : When a suspect says they saw 
% another person
saw(gordon_bleu, pierre_sonnel).
saw(pierre_sonnel, gordon_bleu).
saw(fab_ullateur, charles_hatant).

% wears(P, O). : The clothes and objects on the suspects

% Gordon Bleu
wears(gordon_bleu, cook_uniform__g).

% Sally Riez
wears(sally_riez, trousers__s).
wears(sally_riez, top__s).

% Pierre Sonnel
wears(pierre_sonnel, trousers__p).
wears(pierre_sonnel, shirt__p).

% Charles Hatant
wears(charles_hatant, trousers__c).
wears(charles_hatant, woollen_jumper__c).

% Fab Ullateur
wears(fab_ullateur, jeans__f).
wears(fab_ullateur, shirt__f).

% Klint Yhan
wears(klint_yhan, pyjamas__k).

% smoker(P). : The persons that smokes 
% (thex can live a cigarette butt in a location)
smoker(gordon_bleu).
smoker(charles_hatant).

% lost(P, O). : The objects lost by the suspects
lost(sally_riez, necklace__s).
lost(gordon_bleu, chef_hat__gb).
lost(klint_yhan, suit).
lost(klint_yhan, ring).
lost(fab_ullateur, watch).
lost(charles_hatant, glasses).


% found_in(O, L). : The objects (clues) found_in 
% each location

% Basement
found_in(pistol, basement).
found_in(bag_of_flour, basement).
found_in(cigarette__1, basement).
found_in(glasses, basement).

% restaurant
found_in(necklace__s, restaurant).
found_in(napkin, restaurant).

% Kitchen
found_in(chef_hat__gb, kitchen).
found_in(apron, kitchen).

% Toilets
found_in(newspaper, toilets).

% Boudoir
found_in(handkerchief, boudoir).

% Dick's bedroom
found_in(account_book, dick_bedroom).
found_in(mail, dick_bedroom).

% Room 1
found_in(suit, room_1).
found_in(ring, room_1).

% Room 2
found_in(watch, room_2).
found_in(tie, room_2).

% number of proofs required to prosecute 
% a suspect, here at least two
required_nbr_proofs(N) :- N>=2.

%%%%%%%%%% Logical Rules %%%%%%%%%%

%%%       Finding the murderer       %%%

% supect(P).

% all the persons (except the victim) who hav not 
% been proved not guilty yet
suspect(P):- person(P), \+victim(P), \+not_guilty(P).


% not_guilty(P).

% all the persons with an alibi
not_guilty(P) :- has_alibi(P).


% murderer(P).

% if someone is the only suspect left
% they are the murderer
murderer(P) :- forall(suspect(S), S=P).


% was_in(P, L).

% a person can be in only one location
was_in(P, L) :- person(P), forall(location(X), X=L). 

% someone who lost an object in 
% a location was in this location
was_in(P, L) :- lost(P, O), found_in(O, L).

% if someone was seen by a person who tells 
% the truth, they were in the same location
was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L).


% liar(P). and tells_the_truth(P).

% if someone was not where they pretended 
% to be the persone lies
tells_the_truth(P) :- pretended_location(P, L), was_in(P, L).

% if someone is not telling the truth
% they lie
liar(P) :- \+tells_the_truth(P).


% has_alibi(P)

% someone who was in a location different 
% from the crime scene has an alibi
has_alibi(P) :- was_in(P, L), \+crime_scene(L).


%%%          Proofs system         %%%

% is_proof_against(O, P).

% an object lost by someone on the 
% crime scene is a proof against them
is_proof_against(O, P) :- lost(P, O), found_in(O, L), crime_scene(L).

% if the victim doesn't smoke, a cigarette butt on the 
% crime scene is a proof against smoking suspects
is_proof_against(M, P) :- cigarette_butt(M), found_in(M, L), crime_scene(L), smoker(P), victim(V), \+smoker(V).


% list_of_proof(L). : Whe verify that every element 
% of the list is a proof against the murderer

list_of_proofs([]).
list_of_proofs([H|L]) :- is_proof_against(H, P), murderer(P), list_of_proofs(L).


% number_of_proofs(L, N). : Length of a list of proofs

number_of_proofs([], 0).
number_of_proofs([_|L], Z) :- list_of_proofs(L), number_of_proofs(L, Y), Z #= Y+1.


% can_be_prosecuted(P, L).

% if we have at least the minimum number of proofs 
% required against the murderer they can be prosecuted
can_be_prosecuted(P, L) :- murderer(P), list_of_proofs(L), number_of_proofs(L,N), required_nbr_proofs(N).