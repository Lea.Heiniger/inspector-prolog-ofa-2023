# Scenario 2 : Murder in the basement  


Dick Raicteur is found murdered in the basement of the small inn where he runs the business. The Inn had three floors: the ground floor housed the **restaurant**, the **kitchen** and the **toilets**; the first floor contained the **boudoir**, Dick's **bedroom** and two rooms for rent (**room 1** and **room 2**); and the **basement** was used to store supplies. Everyone present on the premises that evening is under suspicion.  
Once you have identified the murderer, you will also need to find at least two pieces of evidence against them in order to take them to court.  

## The suspects:  

- **Gordon Bleu** (employee): He smokes. He wears a cook's uniform. He says that at the time of the crime he was in the kitchen with Pierre.  
- **Sally Riez** (employee): She wears black top and trousers. She lost her necklace. She says that at the time of the crime she was tidying up the restaurant.  
- **Pierre Sonnel** (employee): He wears black trousers and shirt. He says that at the time of the crime he was in the kitchen with Gordon.  
- **Charles Hatant** (accountant and Dick's friend): He smokes. He wears blue trousers, a grey woollen jumper and glasses (which he has lost). He says that at the time of the crime he was in the boudoir (motive: falsifying accounts).
- **Fab (Fabien) Ullateur** (family member): He wears jeans and a white shirt. He tells you that he lost his watch. He says that at the time of the crime he was in the boudoir where he saw Charles (lies to protect Charles, he was actually in bedroom 2).  
- **Klint Yhan** (rents room no 1): He wears green pyjamas and tells you that he lost his wedding ring. He says he was in his room at the time of the crime.  

## Clues (by room) :

- **Basement** : a pistol (murder weapon), a bag of flour, a cigarette butt and glasses.  
- **Restaurant** : a necklace with the letter S hanging from it and a beige napkin.  
- **Kitchen** : a chef's hat with the initials GB, a knife and a black apron.  
- **Toilets** : a newspaper from the previous day.  
- **Boudoir** : a blue handkerchief.  
- **Dick's bedroom** : the account book of the Inn and Dick's mail.  
- **Room 1** : a black suit with initials K.Y. embroidered inside and a silver ring.
- **Room 2** : a brown watch and a blue tie.