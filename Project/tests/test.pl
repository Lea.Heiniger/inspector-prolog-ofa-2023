% Inspector prolog
% Joao Costa DaQuinta, Lea Heiniger



% This file is used to perform prolog tests
% mainly on integers and the function call()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Resources :
% https://www.metalevel.at/prolog
% https://www.let.rug.nl/bos/lpn//lpnpage.php?pageid=online
% semantique des langages informatiques chapitre 2

% natural integers
nat(0).
nat(s(N)) :- nat(N).

addition(X, 0, X).
addition(s(X), Y, s(Z)) :- addition(X, Y, Z).
addition(X, Y, Z) :- addition(Y, X, Z).

% ?- addition(X, 0, Z). %%%%%%
% ?- addition(s(0), s(s(0)), Z).

% just some tests
:- use_module(library(clpfd)).

len([], 0).
len([H|L], Z) :- len(L, Y), Z #= Y+1.

% ?- len([a,b,c], X).

% ?- lent(L, 0+1+1).
% ?- len(L, 2).

% tests on boolean

is_true(true).
% extend for cases with boolean exp as parameter


not(X, false) :- is_true(X).
% not(X, true) :- ...


and(X, Y, true) :- is_true(X), is_true(Y).
% and(false, Y, false).
% and(X, false, false).
% ...


or(X, Y, true) :- is_true(X); is_true(Y).
% ...

% predicate as parameter : call()

% ?- call(addition(), X, 0, X). 
% ?- call(addition(), s(s(0)), s(0), s(s(s(0)))).

% ?- call(len(), [a,b,c], 3). %%%%
% ?- call(len(), [a,b,c], X). %%%%

% ?- call(not(), true, X).

test_11(A, B, C, D, E, F, G, H, I, J, K) :- A.
test_7(A, B, C, D, E, F, G) :- A.

% ?- call(test_11(), true, B, C, D, E, F, G, H, I, J, K). -> more than 8 parameters
% ?- call(test_7(), true, B, C, D, E, F, G).

