% Inspector prolog
% Joao Costa DaQuinta, Lea Heiniger



% This file is used to perform tests 
% while we developp the fact database.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% introduction of individuals
person(joao).
person(lea).
person(didierBuchs).
person(damien).
person(jean).
person(charles).

% victim definition
victim(joao).

% object()
object(mouchoir).
object(porte_monnaie).

% has_color(O, C)

% location()
location(salle_318).
location(salle_316).
location(local_aei).
location(batelle_RDC).

crime_scene(local_aei).

suspect(P):- person(P), \+victim(P), \+not_guilty(P).

not_guilty(P) :- has_alibi(P).

murderer(P) :- forall(suspect(S), S=P).
murderer(lea).

% wears(P, O)

% smoker(P)
%smoker(jean).
%smoker(charles).
smoker(lea).
%smoker(joao).

%megot(m1).
megot(m2).

% pretended_location(P, L)
pretended_location(lea, salle_316).
pretended_location(didierBuchs, salle_318).
pretended_location(damien, salle_316).
pretended_location(jean, salle_316).
pretended_location(charles, salle_316).


% saw(P1, P2)
saw(damien, didierBuchs).

% lost(P, O)
lost(damien, mouchoir).
lost(lea, porte_monnaie).

% found_in(O, L)
found_in(mouchoir, salle_318).
found_in(porte_monnaie, local_aei).
%found_in(m1, salle_316).
found_in(m2, local_aei).

% someone who smokes could be in a room where a megot is found
could_be_in(P, L) :- smoker(P), found_in(M, L), megot(M).
% IDEA TO LINK could_be_in to was_in :
% we have to put a restriction : each smoker leaves EXACTLY one megot 
% behin them (other wise the logical deduction is to limited to deduce alibi)
% Problem : infinite recursion if we have two megot in the same location and
% only one person already linked to the location


% a person can be in only one location.
was_in(P, L) :- person(P), forall(location(X), X=L). 

% someone who lost an object in a location was in this location.
was_in(P, L) :- lost(P, O), found_in(O, L).

% if someone was seen by a not guilty person, they were in the same location.
was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L).

% if someone was not where they pretended to be the persone lies
tells_the_truth(P) :- pretended_location(P, L), was_in(P, L).
liar(P) :- \+tells_the_truth(P).

% someone who was in a location different from the crime scene has an alibi.
has_alibi(P) :- was_in(P, L), \+crime_scene(L).

is_proof_against(O, P) :- lost(P, O), found_in(O, L), crime_scene(L).
is_proof_against(M, P) :- megot(M), found_in(M, L), crime_scene(L), smoker(P), victim(V), \+smoker(V).

list_of_proofs([]).
list_of_proofs([H|L]) :- is_proof_against(H, P), murderer(P), list_of_proofs(L).

:- use_module(library(clpfd)).

number_of_proofs([], 0).
number_of_proofs([_|L], Z) :- list_of_proofs(L), number_of_proofs(L, Y), Z #= Y+1.

min(X) :- X>=1.
can_be_prosecuted(P, L) :- murderer(P), list_of_proofs(L), number_of_proofs(L,N), min(N).

