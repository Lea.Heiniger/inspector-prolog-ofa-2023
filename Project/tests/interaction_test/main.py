from pyswip import Prolog, Query
import pre_done_text as pdt


pdt.intro_call()
"""
prolog = Prolog()
prolog.consult("scenario1.pl")  # Replace with the path to your Prolog file

# Find all suspects
persons = list(prolog.query("person(P)"))
suspects = [person["P"] for person in persons]

# Exclude victims from the suspects
victims = list(prolog.query("victim(P)"))
suspects = [s for s in suspects if s not in [victim["P"] for victim in victims]]


def main_question():
	possible = [1,2,3,4,5]
	print("You can run any of the following commands:")
	print("1 - list of all objects, colors, owners, status")
	print("2 - locations and crime scene location")
	print("3 - list of all distinctions")
	print("4 - list of all clues")
	print("5 - list of all queries - and use it")
	input_ = int(input("your choice : "))
	if input_ in possible:
		print()
		return input_
	else:
		print("not valid input")
		print()
		return main_question()


def commands():
	print("Here is a list of possible commands: P,S=Person, L=Location, O=Object")
	print("1 - was_in(P, L) -- Person in Location")
	print("2 - was_in(S, L) -- Person lost Object in Location => was there")
	print("3 - was_in(S, L) -- Person saw Person in Location => same location")
	print("4 - has_alibi(P) ")
	print()
		
		
def run_main_question(i):
	if i == 1:
		objects = list(prolog.query("object(O)"))
		for o in objects:
			object_name = o["O"]
			owner = list(prolog.query(f"wears(P, {object_name})"))
			color = list(prolog.query(f"has_color({object_name}, C)"))
			status = list(prolog.query(f"lost(P, {object_name})"))
			owner_name = owner[0]["P"] if owner else "---"
			object_color = color[0]["C"] if color else "---"
			object_status = "Lost" if status else "Not Lost"
			print(f"Object: {object_name:<17} Owner: {owner_name:<13} Color: {object_color:<10} Status: {object_status:<10}")
		print()
		
	elif i == 2:
		locations = list(prolog.query("location(L)"))
		crime_scene = list(prolog.query("crime_scene(L)"))
		crime_scene = crime_scene[0]["L"] if crime_scene else "None"
		for l in locations:
			location_name = l["L"]
			is_crime_scene = "Yes" if location_name == crime_scene else "No"
			print(f"Location: {location_name:<15} Is Crime Scene: {is_crime_scene:<3}")
		print()
		
	elif i == 3:
		distinctions = list(prolog.query("has_distinction(P, D)"))
		for d in distinctions:
			person_name = d["P"]
			distinction_name = d["D"]
			print(f"Person: {person_name:<13} Distinction: {distinction_name:<15}")
		print()
	
	elif i == 4:
		clues = list(prolog.query("found_in(O, L)"))
		for c in clues:
			clue_name = c["O"]
			location_name = c["L"]
			owner = list(prolog.query(f"wears(P, {clue_name})"))
			owner_name = owner[0]["P"] if owner else "---"
			print(f"Clue: {clue_name:<17} Location: {location_name:<15} Owner: {owner_name:<13}")
		print()
	
	elif i == 5:
		commands()
		
	
print("Initial Suspects:", suspects)

while len(suspects) > 1:
	run_main_question(main_question())
"""
	
	
	
	
	
