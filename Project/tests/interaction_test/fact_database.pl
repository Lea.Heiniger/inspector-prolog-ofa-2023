
% person()

% Exemple
%person(lea).
%person(joao).

% object()

% location()

%Exemple
%location(salle_318).
%location(batelle_RDC).

%%%%%%%%%%% Rules

% Global

% supect()
suspect(X):- person(X), \+victim(X), \+not_guilty(X).

% not_guilty()
not_guilty(X) :- has_alibi(X).
% not_guilty(X) :- clue(Y), proof(X,Y). ?

murderer(X) :- forall(suspect(Y), Y=X).

% Usable by player

% not working properly
%was_in(P, L) :- person(P), forall(location(X), X=L). 
%was_in(S, L) :- lost(S, O), found_in(O, L).
%was_in(S, L) :- not_guilty(P), was_in(P, L), saw(P, S).

%has_alibi(P) :- was_in(P, L), \+crime_scene(L).

%%%%%%%%%%%%%%%%%%%%% Define a case %%%%%%%%%%%%%%%%%%%%% 

% victim(person)

% Exemple
%victim(didier).

% crime_scene(location)

% Exemple
%crime_scene(salle_318).

%% object location and description

% has_color(object)
% found_in(object, location)

%% person description

% wears(person, object)
% has_distinction(person, *name of distinction usefull to the secnario*)

%% person acctions

% lost(person, object)
% saw(person, person)
