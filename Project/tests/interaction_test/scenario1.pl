% person()
person(monsieur_X).
person(jean_thy).
person(amy_khale).
person(sam_patick).
person(chris_minel).

% victim(person)
victim(monsieur_X).

% object()
object(megot).
object(chapeau).
object(boucle_oreille_o).
object(couteau).
object(montre).
object(boucle_oreille_a).
object(mouchoir).
object(liste_courses).
object(lunettes).

object(manteau_j).
object(pantalon_j).
object(lunettes_a).
object(robe_a).
object(costume_p).
object(casquette_c).
object(pull_c).
object(jeans_c).

has_color(chapeau, vert).
has_color(boucle_oreille_o, dore).
has_color(montre, noir).
has_color(mouchoir, bleu).
has_color(boucle_oreille_a, argente).

has_color(manteau_j, noir).
has_color(pantalon_j, brun).
has_color(robe_a, bleu).
has_color(costume_p, vert).
has_color(casquette_c, noir).
has_color(pull_c, blanc).

% location()
location(chambre).
location(cuisine).
location(salon).
location(bibliotheque).

crime_scene(chambre).

%%%

suspect(P):- person(P), \+victim(P), \+not_guilty(P).

not_guilty(P) :- has_alibi(P).

murderer(P) :- forall(suspect(S), S=P).

%%%%%%%%%%%%%%%%%% Description Suspects %%%%%%%%%%%%%%%%%%%%%%

% Jean Thy
wears(jean_thy,manteau_j).
wears(jean_thy, pantalon_j).
% says he was at the library.

% Amy Khale
wears(amy_khale, robe_a).
% says she was in the livingroom.

% Sam Patick
wears(sam_patick, costume_p).
% says he was at the library.

% Chris Minel
wears(chris_minel, casquette_c).
wears(chris_minel, pull_c).
wears(chris_minel, jeans_c).
% says he was in the kitchen.

% how to describe pretended locations of the suspects ? (must be proved to inocent them)

% fumeurs
has_distinction(amy_khale, fumeur).
has_distinction(sam_patick, fumeur).
has_distinction(chris_minel, fumeur).

% how to link "fumeur" to "megot"


saw(sam_patick, jean_thy).
saw(jean_thy, sam_patick).


% lost objects
lost(amy_khale, boucle_oreille_a).
lost(sam_patick, lunettes).

%%%%%%%%%%%%%%%%%% Description clues %%%%%%%%%%%%%%%%%%%%%%%%%

% Chambre 
found_in(megot, chambre).
found_in(chapeau, chambre).
found_in(boucle_oreille_o, chambre).

% Cuisine
found_in(couteau, cuisine).
found_in(montre, cuisine).

% Salon
found_in(boucle_oreille_a, salon).
found_in(mouchoir, salon).

% Bibliotheque
found_in(liste_courses, bibliotheque).
found_in(lunettes, bibliotheque).

%%%%%%%%%%%%%%%%%%%%% Rules %%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Recurtion Error : first was_in is not sufficient (steel searching other locations)

% a person can be in only one location.
was_in(P, L) :- person(P), forall(location(X), X=L). 

% someone who lost an object in a location was in this location.
was_in(S, L) :- lost(S, O), found_in(O, L).

% if someone was seen by a not guilty person, they were in the same location.
was_in(S, L) :- saw(P, S), not_guilty(P), was_in(P, L).

% someone who was in a location different from the crime scene has an alibi.
has_alibi(P) :- was_in(P, L), \+crime_scene(L).

% TO ADD :
%    - Define property "fumeur" (a "fumeur" can leave a "megot" behind)
%      and globaly define "distinctions" mecanics.
%    - Find a way to describe pretended locations of suspects.
%    - Find a way to deduce what the murder weapon is.

% IDEAS :
%    - Implement a mecanism of evidences that the player will have 
%      to present to prosecute the murderer ?
%    - mobile ?
