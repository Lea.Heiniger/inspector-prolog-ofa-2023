%%% facts database

% introduction of individuals
person(joao).
person(lea).
person(didierBuchs).
person(jean).

% victim definition
victim(joao).

% possible locations
room(lounge).
room(kitchen).

% locations of persons
in_room(joao, lounge).
in_room(lea, lounge).
in_room(didierBuchs, kitchen).

%%% rules

% a suspect is a persone that isnt a victim
suspect(X):- person(X), \+victim(X).

% same room
same_room(X,Y):- in_room(X,Z), in_room(Y,Z).

% suspects who were not in the same room as victim have alibi
%has_alibi(X):- suspect(X), victim(Y), \+same_room(X,Y).
%celle ci bug

distinction(fumeur, megot).

has_distinction(P, D) :- person(P), distinction(D).

% link too proof ?

has_distinction(jean,fumeur).




