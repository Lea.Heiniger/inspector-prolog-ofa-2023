intro_1 = "You just arrived on the crime scene! You are the main inspector in this case, however you have a partner!"
intro_2 = "Sherlock had John, Batman had Robin. Well, you have ... a Owl..."
intro_3 = "Its name is Prolog the Owl and it possesses incredible powers of logical deduction."
intro_4 = "It is up to you to use its powers well. Just know that if you want to get home early today, it is *IMPERATIVE* that you work with your partner!!" 

def intro_call():
	print(intro_1)
	print(intro_2)
	print(intro_3)
	print(intro_4)
	print()
