# Import necessary modules
from pyswip import Prolog, Query
from multiprocessing import Pool
import pre_done_text as pdt

# Initialize Prolog
prolog = Prolog()

# Initialize empty lists to store introduced functions and done queries
introduced_functions = []
queries_done = []

# Initialize second_step to False
second_step = False
been_here_1 = False
been_here_2 = False

# Ask the user which level they want to play and store the input in variable 'scenario'
scenario = int(input("Which level do you want to play : "))
print()

# Depending on the user's choice, load the corresponding Prolog scenario
if scenario == 1:
	prolog.consult("scenario1/scenario1.pl")
elif scenario == 2:
	prolog.consult("scenario2/scenario2.pl")
else:
	prolog.consult("scenario0_exemple/exemple.pl")

# Find all persons/suspects using a Prolog query and store the results in a list 'suspects'
persons = list(prolog.query("person(P)"))
suspects = [person["P"] for person in persons]
suspects_ = [person["P"] for person in persons]

# Exclude victims from the list of suspects using another Prolog query
victims = list(prolog.query("victim(P)"))
suspects = [s for s in suspects if s not in [victim["P"] for victim in victims]]
suspects_ = [s for s in suspects_ if s not in [victim["P"] for victim in victims]]

# Find all locations using a Prolog query and store the results in a list 'locations'
locations_query = list(prolog.query("location(L)"))
locations = [result["L"] for result in locations_query]

# Define additional user interaction 
additional_options = {
	len(locations) + 1: "Clues related to suspects",
	len(locations) + 2: "Get testimonies from suspects",
	len(locations) + 3: "Send a query to Prolog",
	len(locations) + 4: "Get help with Prolog"
}


def intro_scenario():
	# Print introductory lines
	print("\nOk, lets start: \n\n")

	# Query the Prolog database for persons, victims, locations and crime scenes
	persons_query = list(prolog.query("person(P)"))
	victim_query = list(prolog.query("victim(V)"))
	locations_query = list(prolog.query("location(L)"))
	crime_scene_query = list(prolog.query("crime_scene(C)"))

	# Get the first victim and crime scene from the queries, if any
	victim = victim_query[0]["V"] if victim_query else None
	crime_scene = crime_scene_query[0]["C"] if crime_scene_query else None

	# Get the list of suspects, which is the list of persons excluding the victim
	suspects = [person["P"] for person in persons_query if person["P"] != victim]
	# Get the list of locations
	locations = [location["L"] for location in locations_query]

	# Print information about the victim, crime scene, suspects, and locations
	print(f'The victim in this case is {victim}.')
	print(f'The crime occurred at {crime_scene}.')
	print(f'\nThere are {len(suspects)} suspects in this case:')
	for suspect in suspects:
		print(f'- {suspect}')
	print(f'\nThere are {len(locations)} locations to investigate:')
	for location in locations:
		print(f'- {location}')
	print()

def tutorial_pretended_location(queries_done):
    print("\nFirst thing is to inquire where every suspect was at the time of the murder ")
    print("Press '1' and following question will be sent to your collegue Owl : ")
    input_ = int(input("pretended_location(P, L) -- where P is a Person, and L a Location : "))
    # If user presses 1, execute Prolog query 'pretended_location(P, L)' and print out the results
    if input_ == 1:
        pretended_location_query = list(prolog.query("pretended_location(P, L)"))
        for result in pretended_location_query:
            person = result['P']
            location = result['L']
            print(f'{person} pretendeds to be at {location}.')
            # Append the executed query to the list of done queries, replacing 'P' and 'L' with actual person and location
            queries_done.append("pretended_location(P,L)".replace("P",person).replace("L",location))
    else:
        tutorial_pretended_location()
    print("\n")
    print("You just colaborated with Owl to learn everyones pretended location, congrats!!")
    print("Lets continue! \n")

    # Return the list of done queries
    return queries_done

# used to print options of the main menu
def print_menu_options(locations, additional_options):
	for i, location in enumerate(locations, start=1):
		print(f"{i} -- inspect {location}")
	for option, description in additional_options.items():
		print(f"{option} -- {description}")


def find_objects_in_location(location):
    # Query the Prolog database to find objects that were found in the specified location
    objects = list(prolog.query(f"found_in(O, {location})"))
    
    # Print out the location being investigated
    print(f"\nObjects found in {location}:")

    # If there are objects found in the location
    if objects:
        # Iterate through the objects
        for o in objects:
            # Retrieve the object name
            object_name = o["O"]
            # Query the Prolog database to find the owner of the object
            owner = list(prolog.query(f"wears(P, {object_name})"))
            # Query the Prolog database to find the color of the object
            color = list(prolog.query(f"has_color({object_name}, C)"))
            # Query the Prolog database to find any additional detail of the object
            detail = list(prolog.query(f"has_detail({object_name}, D)"))
            
            # Retrieve the owner name, color and detail of the object if they exist
            owner_name = owner[0]["P"] if owner else "---"
            object_color = color[0]["C"] if color else "---"
            object_detail = detail[0]["D"]  if detail else "---"

            # Print the information about the object in a structured format
            print(f"Object: {object_name:<17} Owner: {owner_name:<13} Color: {object_color:<10} Detail: {object_detail}")
    else:
        print("No objects found in this location.")
	
def find_clues_for_suspects():
    # Query the Prolog database to get all suspects
    suspects = list(prolog.query("suspect(S)"))

    # Iterate through all suspects
    for s in suspects:
        # Get the suspect's name
        suspect_name = s["S"]

        # Print out the name of the suspect whose clues we're looking for
        print(f"\nLooking for clues related to: {suspect_name}")

        # Query the Prolog database to get objects that the suspect has lost
        objects = list(prolog.query(f"lost({suspect_name}, O)"))

        # If the suspect has lost any objects
        if objects:
            # Iterate through all the lost objects
            for o in objects:
                # Get the name of the object
                object_name = o["O"]

                # Query the Prolog database to get the location where the object was found
                object_location = list(prolog.query(f"found_in({object_name}, L)"))

                # Get the location name if it exists
                object_location_name = object_location[0]["L"] if object_location else "---"

                # Print the clue (object) and the location where it was found
                print(f"Clue: {object_name:<17} Found in location: {object_location_name:<13}")
        else:
            print("No clues found for this suspect.")


def find_clues_for_uncleared_():
    # Initialize a list of people who are uncleared (suspects)
    persons = suspects

    # Iterate through all uncleared people
    for person_name in persons:
        # Print out the person's name and the objects they own
        print(f"\n{person_name}'s objects:")

        # Query the Prolog database to get objects that the person owns
        objects = list(prolog.query(f"wears({person_name}, O)"))

        # If the person owns any objects
        if objects:
            # Iterate through all owned objects
            for o in objects:
                # Get the name of the object
                object_name = o["O"]

                # Query the Prolog database to get the color of the object
                color = list(prolog.query(f"has_color({object_name}, C)"))
                # Query the Prolog database to check if the object is lost
                status = list(prolog.query(f"lost({person_name}, {object_name})"))

                # Get the object color if it exists
                object_color = color[0]["C"] if color else "---"
                # Determine if the object is lost or not
                object_status = "Lost" if status else "Not Lost"

                # Print the object's name, color, and status (lost or not lost)
                print(f"Object: {object_name:<17} Color: {object_color:<10} Status: {object_status:<10}")
        else:
            print("This person does not own any objects.")


def get_testimonies():
    # Initialize a list of people who are uncleared (suspects)
    people = suspects

    # Iterate through all uncleared people
    for person_name in people:
        # Print out the person's name and the testimonies related to them
        print(f"\nTestimonies and observations related to: {person_name}")

        # Query the Prolog database to get people that the current person has seen
        saw_people = list(prolog.query(f"saw({person_name}, P2)"))
        # Query the Prolog database to check if the person is a smoker
        is_smoker = list(prolog.query(f"smoker({person_name})"))
        # Query the Prolog database to get objects that the person has lost
        lost_objects = list(prolog.query(f"lost({person_name}, O)"))

        # If the person has seen any other people
        if saw_people:
            # Iterate through all people the current person has seen
            for s in saw_people:
                # Print who the person saw
                print(f"{person_name} saw {s['P2']}")

        # If the person is a smoker
        if is_smoker:
            # Print that the person is a smoker
            print(f"{person_name} is a smoker")

        # If the person has lost any objects
        if lost_objects:
            # Iterate through all lost objects
            for o in lost_objects:
                # Get the name of the object
                obj = o['O']
                # Print what object the person lost
                print(f"{person_name} lost {obj}")

def execute_queries(queries_done, suspects):
    # Print some newlines for better output formatting
    print("\n\n")

    # Iterate through all introduced functions and print them with their corresponding index
    for i, function in enumerate(introduced_functions, 1):
        print(f"{i} -- {function}")

    # Prompt user to choose a function to execute
    choice = input("\nWhat do you want to do? ")

    # If user's choice is a digit and the choice is within the range of available functions
    if choice.isdigit() and int(choice) in range(1, len(introduced_functions)+1):
        # Get the chosen function
        chosen_function = introduced_functions[int(choice) - 1]

        # Execute the chosen function and update the queries done and suspects
        queries_done, suspects = execute_queries_(chosen_function, queries_done, suspects)

        # Return the updated queries_done and suspects lists
        return queries_done, suspects


def filter(queries_done, t):
    # Initialize an empty list to store filtered queries
    queries_done_ = []

    # Iterate through all queries that have been done
    for q in queries_done:
        # If the query starts with the provided string 't'
        if q.startswith(t):
            # Add the query to the filtered list
            queries_done_.append(q)

    # Return the filtered list of queries
    return queries_done_


def list_(T, queries_done):
	# depending on T -> we know what axiom the user wants to use
	# depending on the query different actions are taken
	# but basically if we are looking for an Object, we query the prolog for every object
	# print them, and let the user pick one

	# for non trivial queries lie "was_in(P, L)" we look through the already done queries
	# this way we wont let the user just skip steps, to use was_in(x,y) in has_alibi(x)
	# he first needs to prove was_in(x,y)
	match T:
		case "O":
			objet_query = list(prolog.query("object(O)"))
			objets = [objet["O"] for objet in objet_query]
			for i, obj in enumerate(objets, start=1):
				print(f"{i} -- {obj}")
			choice = input("\nWhat Object as " + T + " : ")
			return objets[int(choice) - 1]
		
		case "L":
			locations_query = list(prolog.query("location(L)"))
			locations = [location["L"] for location in locations_query]
			for i, loc in enumerate(locations, start=1):
				print(f"{i} -- {loc}")
			choice = input("\nWhat Location as " + T + " : ")
			return locations[int(choice) - 1]
		
		case "pretended_location(P, L)":
			queries_done_ = filter(queries_done, "pretended_location")
			for i, loc in enumerate(queries_done_, start=1):
				print(f"{i} -- {loc}")
			choice = input("\nWhat query as " + T + " : ")
			return queries_done_[int(choice) - 1]
		
		case "was_in(P, L)":
			queries_done_ = filter(queries_done, "was_in")
			for i, loc in enumerate(queries_done_, start=1):
				print(f"{i} -- {loc}")
			choice = input("\nWhat query as " + T + " : ")
			return queries_done_[int(choice) - 1]
		
		case "found_in(O, L)":
			queries_done_ = filter(queries_done, "found_in")
			for i, loc in enumerate(queries_done_, start=1):
				print(f"{i} -- {loc}")
			choice = input("\nWhat query as " + T + " : ")
			return queries_done_[int(choice) - 1]
		
		case "saw(P, S)":
			queries_done_ = filter(queries_done, "saw")
			for i, loc in enumerate(queries_done_, start=1):
				print(f"{i} -- {loc}")
			choice = input("\nWhat query as " + T + " : ")
			return queries_done_[int(choice) - 1]
		
		case "lost(P, O)":
			queries_done_ = filter(queries_done, "lost")
			for i, loc in enumerate(queries_done_, start=1):
				print(f"{i} -- {loc}")
			choice = input("\nWhat query as " + T + " : ")
			return queries_done_[int(choice) - 1]
		
		case "tells_the_truth(P)":
			people = suspects_
			for i, person_name in enumerate(people, start=1):
				print(f"{i} -- {person_name}")
			choice = input("\nWhat Person as " + T + " : ")
			T = T.replace("P", people[int(choice) - 1])
			return T

		case default:
			people = suspects_
			for i, person_name in enumerate(people, start=1):
				print(f"{i} -- {person_name}")
			choice = input("\nWhich Person as " + T + " : ")
			return suspects_[int(choice) - 1]
		

def replace_variables(s1, s2):
    args = {}
    s1 = s1.replace(" ","")
    s2 = s2.replace(" ","")
    parts_s1 = s1.split(':-')[1:]
    parts_s2 = s2.split(':-')[1:]
    parts_s1_ = parts_s1[0].split('(')[1:]
    parts_s2_ = parts_s2[0].split('(')[1:]
    for i in range(len(parts_s1_)):
        s1_ = parts_s1_[i].split(")")[0].split(",")
        s2_ = parts_s2_[i].split(")")[0].split(",")
        for j in range(len(s1_)):
            k = s1_[j]
            v = s2_[j]
            if k not in args.keys():
                args[k] = v
    query = s1.split(":-")[0]
    for k in args.keys():
        query = query.replace(k,args[k])
    return query


def execute_prolog_command(com, queries_done, suspects, mode=None):
    # Execute the Prolog command 'com' and iterate over the resulting solutions
    for sol in prolog.query(com):
        # If the command executes successfully, print "TRUE"
        print("TRUE")
        # Add the command to the 'queries_done' list
        queries_done.append(com)
        # Remove duplicates from 'queries_done' list by converting it into a set and then back into a list
        queries_done = list(set(queries_done))
        # If function is running in mode 3
        if mode == 3:
            # Extract the person from the Prolog command 'com'
            person = com.split("(")[1].split(")")[0]
            # If this person is in the list of suspects, remove them
            if person in suspects:
                suspects.remove(person)
        # Return the updated 'queries_done' list and 'suspects' list
        return queries_done, suspects
    else:
        # If the command does not execute successfully, print "FALSE"
        print("FALSE")
        return queries_done, suspects



def build_query(query, text, type, queries_done):
    # Print the original query
    print("QUERY : ", query)

    # Print the message to the user to select a specific entity (person, location, etc.)
    print("\nChoose a "+text+" for "+type+" : ")

    # Call the 'list_' function to display the available entities for the given type (person, location, etc.)
    # The function 'list_' returns the user's selection
    r = list_(type, queries_done)

    # Print the selected entity
    print(type,r)

    # Replace the type placeholder in the query with the selected entity
    query = query.replace(type, r)

    # Return the updated query
    return query



def execute_queries_(chosen_function, queries_done, suspects):
	mode = 1
	match chosen_function:
		# depending on the axiome we need to do different things
		# each case is fully executed in their case
		case "lost(P,O)":
			query = "lost(P,O)"
			query = build_query(query, "Person", "P", queries_done)
			# replaces P with a person 
			query = build_query(query, "Object", "O", queries_done)
			# replaces O with a Object 
			print("\n","QUERY : ", query)

		case "found_in(O,L)":
			query = "found_in(O,L)"
			query = build_query(query, "Object", "O", queries_done)
			query = build_query(query, "Location", "L", queries_done)
			print("\n","QUERY : ", query)

		case "saw(P,S)":
			query = "saw(P,S)"
			query = build_query(query, "Person", "P", queries_done)
			query = build_query(query, "Person", "S", queries_done)
			print("\n","QUERY : ", query)

		case "tells_the_truth(P) :- pretended_location(P, L), was_in(P, L)":
			query_ = "tells_the_truth(P) :- pretended_location(P, L), was_in(P, L)"
			query = "tells_the_truth(P) :- pretended_location(P, L), was_in(P, L)"
			query = build_query(query, "pretended_location query", "pretended_location(P, L)", queries_done)
			query = build_query(query, "was_in query", "tells_the_truth(P)", queries_done)
			print("\n","QUERY : ", query)
			mode = 2

		case "was_in(P, L) :- lost(P, O), found_in(O, L)":
			query_ = "was_in(P, L) :- lost(P, O), found_in(O, L)"
			query = "was_in(P, L) :- lost(P, O), found_in(O, L)"
			query = build_query(query, "lost query", "lost(P, O)", queries_done)
			query = build_query(query, "found_in query", "found_in(O, L)", queries_done)
			mode = 2

		case "was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L)":
			query_ = "was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L)"
			query = "was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L)"
			query = build_query(query, "saw query", "saw(P, S)", queries_done)
			query = build_query(query, "tells_the_truth query", "tells_the_truth(P)", queries_done)
			query = build_query(query, "was_in query", "was_in(P, L)", queries_done)
			mode = 2

		case "has_alibi(P) :- was_in(P, L), \+crime_scene(L)":
			query_ = "has_alibi(P) :- was_in(P, L), \+crime_scene(L)"
			query = "has_alibi(P) :- was_in(P, L), \+crime_scene(L)"
			query = build_query(query, "was_in query", "was_in(P, L)", queries_done)
			query = build_query(query, "crime_scene query", "L", queries_done)
			mode = 3
	
	if mode == 1:
		queries_done, suspects = execute_prolog_command(query, queries_done, suspects)
	elif (mode == 2) or (mode == 3):
		query = replace_variables(query_,query)
		print("\n", query)
		queries_done, suspects = execute_prolog_command(query, queries_done, suspects, mode = mode)
		
	
	return queries_done, suspects


    
pdt.intro_call()
input("press anything to continue \n")
pdt.intro_prolog()
input("press anything to continue \n")
intro_scenario()
input("press anything to continue \n")
queries_done = tutorial_pretended_location(queries_done)
value_to_pass = len(queries_done) + 1




while len(suspects) > 1:
	print()
	print("Here are the suspects : ", suspects)
	print()
	print_menu_options(locations, additional_options)
	print()
	choice = int(input("What do you want to do? "))
	if 1 <= choice <= len(locations):
		location = locations[choice - 1]
		find_objects_in_location(location)
	elif choice == len(locations) + 1:
		find_clues_for_uncleared_()
	elif choice == len(locations) + 2:
		get_testimonies()
	elif choice == len(locations) + 3:
		queries_done, suspects = execute_queries(queries_done, suspects)
		if (value_to_pass > 1) and (second_step == False):
			second_step = True
			additional_options[len(locations) + 5] = "Get even more help with Prolog"
	elif choice == len(locations) + 4:
		if been_here_1 == False:
			introduced_functions.append("lost(P,O)")
			introduced_functions.append("found_in(O,L)")
			introduced_functions.append("saw(P,S)")
		been_here_1 = True
		pdt.intro_prolog_2()
	elif (choice == len(locations) + 5) and (second_step == True):
		if been_here_2 == False:
			introduced_functions.append("tells_the_truth(P) :- pretended_location(P, L), was_in(P, L)")
			introduced_functions.append("was_in(P, L) :- lost(P, O), found_in(O, L)")
			introduced_functions.append("was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L)")
			introduced_functions.append("has_alibi(P) :- was_in(P, L), \+crime_scene(L)")
		been_here_2 = True
		pdt.intro_prolog_3()
	else:
		print("Invalid option. Please try again.")



print("Congratulations ! ")
print("You have successfully alibied everyone but ", suspects[0])
