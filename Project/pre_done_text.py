intro_1 = "You just arrived on the crime scene! You are the main detective in this case, It is the first time you are the main detective on a case. So it is important you impress the boss!!"
intro_2 = "But don't worry. You have a partner!"
intro_3 = "Sherlock had John, Batman had Robin. Well, you have ... an Owl..."
intro_4 = "Its name is \"Prolog the Owl\" and it possesses incredible powers of logical deduction."

prolog_help_1 = "\"Prolog the Owl\" doesn't know everything.."
prolog_help_2 = "He only knows what he's been taught"
prolog_help_3 = "For example, he knows that every detective, owns a monocle!"
prolog_help_4 = "So if you ask \"Prolog the Owl\" if you are a detective it will check weather you own a monocle!\n"
prolog_help_5 = "Don't worry, you do ..."

prolog_capabilities_1 = "Prolog is quite simple. It has functions, and tokens which functions need to work properly!"
prolog_capabilities_2 = "In our case we have 4 types of tokens : "
prolog_capabilities_3 = "\"P\" & \"S\" are persons  -  \"O\" is an object  -  \"O\" is an object\n"
prolog_capabilities_4 = "Which are used by various fucntions:"
prolog_capabilities_5 = "lost(P,O)     -- Person lost Object"
prolog_capabilities_6 = "found_in(O,L) -- Object found in Location"
prolog_capabilities_7 = "saw(P,S)      -- person P saw person S\n"
prolog_capabilities_8 = "Thats all, for now .. "

prolog_capabilities_1_ = "You will now get more functions. However, these are different, these consume other functions as tokens!"
prolog_capabilities_2_ = "There are 4 of them, well, 3 actually : "
prolog_capabilities_3_ = "tells_the_truth(P) :- pretended_location(P, L), was_in(P, L)  --  If Person P pretended to bein Location L, and you can prove it by using was_in, then Person P tells the truth"
prolog_capabilities_4_ = "was_in(P, L) :- lost(P, O), found_in(O, L)                    --  If Person P loses object O, and object O is found in location L, then Person P was in location L"
prolog_capabilities_5_ = "was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L)   --  If Person P sees Person S, and Person P tells the truth, and Person P was in Location L, then Person S was in Location L as well"
prolog_capabilities_6_ = "has_alibi(P) :- was_in(P, L), \+crime_scene(L)                --  If Persopm P was in Location L, which is not the crime Scene, then Person P has an alibi"
prolog_capabilities_7_ = "\nYou won't need to use crime_scene(L) or pretended_location, those were executed found during the tuturial! "
prolog_capabilities_8_ = "However, to use \" was_in(P, L) :- lost(P, O), found_in(O, L)\" -- you need to use \"lost(P, O)\" and \"found_in(O, L)\" before"


def intro_call():
	print(intro_1)
	print(intro_2)
	print(intro_3)
	print(intro_4)
	print()


def intro_prolog():
	print(prolog_help_1)
	print(prolog_help_2)
	print(prolog_help_3)
	print(prolog_help_4)
	print(prolog_help_5)
	print()

def intro_prolog_2():
	print("\n\n\n\n\n\n\n")
	print(prolog_capabilities_1)
	print(prolog_capabilities_2)
	print(prolog_capabilities_3)
	print(prolog_capabilities_4)
	print(prolog_capabilities_5)
	print(prolog_capabilities_6)
	print(prolog_capabilities_7)
	print(prolog_capabilities_8)
	print("\n\n\n\n\n\n\n")


def intro_prolog_3():
	print("\n\n\n\n\n\n\n")
	print(prolog_capabilities_1_)
	print(prolog_capabilities_2_)
	print(prolog_capabilities_3_)
	print(prolog_capabilities_4_)
	print(prolog_capabilities_5_)
	print(prolog_capabilities_6_)
	print(prolog_capabilities_7_)
	print(prolog_capabilities_8_)
	print("\n\n\n\n\n\n\n")
	
	
	
	
	
	
	
	
