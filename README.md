# Inspector Prolog   


Joao Costa Da Quinta, Lea Heiniger 


## Project description 

This project is part of the Concurrence et répartition course during the spring 2023 semester.

Our goal was to create an educational game for learning formal logic. The user takes on the role of a detective who has to solve cases using logical rules on the evidence they have found.  
  
We developed a global template in prolog, the **fact database**, to add new scenarios and we already have two functional scenarios and a basic exemple implemented using the template.  
The **user interaction** part is implemented in python. It introduces the game and prolog to the user before allowing them to play the scenario they want. This part is completely independent of the content of the scenarios, which means that any scenario implemented using the fact database template will work. This and the fact database template allows the content of the game to be easily added or modified, as well as a possible expansion of the project by randomly generating scenarios.  
  
## Files and directories  
  
- **Logbook** : This folder contains the project logbook, a weekly summary of our progress, and the LaTex files and template in the *Latex_files* subfolder.  
  
- **Presentations** : This folder contains the slides from our various presentations over the course of the semester as well as those from our final project presentation. The sub-folder *latex_template* contains a presentation template as well as the files for the current presentation. The images used and the semester plan can be found in the *img* folder.  
  
- **Project** : This file contains the implementation of the project.  
    - Scenarios : The *fact_database.pl* file contains the global fact database, and should be used as a template for scenario implementation. There are three scenarios already implemented in the *scenario0_exemple* (a very basic scenario used as an example), *scenario1* and *scenario2* folders. Each scenario folder contains a description of the scenario in a markdown file and its implementation in prolog.  
      
    - User interaction: The interaction with the user is implemented in the *main.py* file and the *pre_done_text.py* file (a library of texts and functions used when introducing the game).  
      
    - *tests* folder: This folder contains all the files used for testing during the development of the project. The *test.pl* file was used to run prolog tests and refresh our prolog knowledge at the start of the project. *facts_test.pl* is used to run tests on the facts database and to develop new features for the game. The file was mainly used to run tests on small pieces of code, I would often modify it. The folder interaction_test is where I started to create the user interaction, the idead is that I wanted to work in a completely different path from my collegue, to avoid merge errors.  
  
## How to run the game  
Requirements:  
- python 3.10.6  
- SWI-Prolog version 8.4.2 for x86_64-linux  
  
python3 main.py  
  
## Fact database  
The fact database of a scenario is the prolog file that contains all facts describing a scenario as well as the logical rules used for reasoning. We will use code from the exemple scenario (in directory *scenario0_exemple*) as exemple for the explainations.  
  
The library CLP(FD) is used to reason on integers.
```
:- use_module(library(clpfd)).
```

### Case definition

The first two things to define a case are the name of the scenario, defined with *case_name()*, and a small text to introduce the case to the player, defined with *case_introduction()*. Both functions take strings as parameters.  

```
case_name('An OFM project gone wrong').
case_introduction('Joao is found dead in one of Batelle s corridors...').
```
  
We can also set a minimum amount of evidence against the murderer that the user must provide. The default value is 0, but this can be changed to make the scenario more difficult.  
```
required_nbr_proofs(N) :- N>=0.
```
The use of this predicate will be explained in more detail with the proof system in the section on logic rules.   
  
A case is then defined by three main elements: the **people**, the **objects** and the **places** involved. In the following sub-sections, we will see how these three elements are defined and the predicates that apply to them.  
In order to be able to parse the names of atoms for the text seen by the user, we have used the following convention: spaces are represented by undescores and we place two undescores in front of the identifiers that distinguish two objects. For the time being, we have kept the prolog names in the texts (so that the player knows how to refer to each object in the queries), but an improvement could be to use parsing in the texts while giving the prolog names to the user in another way.
```
object(jeans__l).
object(jeans__d).
```
Both objects are jeans the letters after the underscores are used to distinguish the two objects in prolog.  
  
#### 1. Persons
  
The persons involved in a case are defined using the predicate *person()*.
```
person(joao).
person(damien).
person(pr_buchs).
person(mysterious_student).
```
  
The murder victim is defined using the predicate *victim(Person)*.
```
victim(joao).
``` 
  
Some persons can smoke they are defined using the predicate *smoker(Person)*.
```
smoker(mysterious_student).
```
Smokers can leave cigarette butts behind. For the moment, this property is only used for the proof system (described in the section on logical rules). But in the *tests/facts_test.pl* file there are some tests carried out trying to link it to the alibi system through a function called *could_be_in(Person, Location)*.
  

A person can be linked to an object (see sub-section 2) either because they are wearing it, or because they have lost it.   
We can describe what a person is wearing by using the predicate *wears(Person, Object)*.
```
wears(damien, jeans__d).
wears(damien, teeshirt__d).
```
  
And the objects they lost are linked to them by the predicate *lost(Person, Object)*.
```
lost(mysterious_student, wallet).
```
  

Each person (with the exception of the murder victim) will tell the user where they were (see sub-section 3 on locations) at the time of the crime. Of course, the user has to check if this is true, but these supposed locations are defined using the predicate *pretended_location(Person, Location)*.
```
pretended_location(damien, room_318).
```
  
Finaly a person can tell the user that they saw another person at their location. This is defined using the predicate *saw(Person1, Person2)*.
```
saw(damien, pr_buchs).
saw(pr_buchs, damien).
```
In this exemple *damien* and *pr_buchs* saw each other but we can also have a case where person1 saw person2 but not the opposit.

#### 2 .Objects

Objects are defined using the predicate *object()*.
```
object(wallet).
object(laptop).
object(jeans__d).
object(teeshirt__d).
```
  
Cigarette butts are a specific type of object (since we need to link them to smokers). We defin them using the predicate *cigarette_butt()*.
```
cigarette_butt(cigarette).
```
  
As for the persons, an object can be described. The first way is to defin its color using the *has_color(Object, String)* predicate.
```
has_color(wallet, 'blue').
has_color(teeshirt__d, 'grey').
```
  
An object can also have a specific detail, in the form of a small text that the user will discover when looking at the object. These are defined using the predicate *has_detail(Object, String)*.
```
has_detail(laptop, 'The username on the login screen is Damien.').
```
  
  
Finally, certain objects (clues) will be discovered by the user at a specific location (see sub-section 3). This is defined by the predicate *found_in(Object, Location)*.
```
found_in(laptop, room_318).
found_in(cigarette, local_aei).
```

#### 3. Locations

The user can search the various locations for objects (see the *found_in* predicate in the previous sub-section). They will also have to check that the suspects were actually where they said they were (see the *pretended_location* predicate in sub-section 1) in order to clear them.  
The different locations in a case are defined using the *location()* predicate.
```
location(room_318).
location(corridor).
location(local_aei).
```
We also have to define the location where the crime occured using the predicate *crime_scene(Location)*.  
```
crime_scene(corridor).
```

### Logical rules

To reason over the scenarios, we have implemented logical rules. These rules are independent of the scenario and do not change from one case to another.  
We have two main aspects: the search for the murderer and the proof system.  

#### Finding the murderer

To find the murderer, you need to draw up a list of **suspects**. Initially, this is all the persons except the victim. Using other rules, we can then define certain suspects as not guilty and remove them from the list of suspects.  
```
suspect(P):- person(P), \+victim(P), \+not_guilty(P).
```

To be able to say that a person is not guilty, we first need to know **where they were**. We define the rule *was_in(Person, Location)* as follows:  
- A person can only be in one location.  
- If a person has lost an object, they were at the same location as where the object was found.  
- If a person was seen by someone who is telling the truth, they were in the same location.  
```
was_in(P, L) :- person(P), forall(location(X), X=L). 
was_in(P, L) :- lost(P, O), found_in(O, L).
was_in(S, L) :- saw(P, S), tells_the_truth(P), was_in(P, L).
```
  
Once we know where a person was, we can compare it to where they claimed to be. If the two places are identical, the person is **telling the truth**, otherwise **they are lying**.
```
tells_the_truth(P) :- pretended_location(P, L), was_in(P, L).
liar(P) :- \+tells_the_truth(P).
```
As we saw, if someone is lying, we can't trust them to determine where someone else was.  

A person **not guilty** is a person who has an alibi. And someone **has an alibi** if they were somewhere other than the crime scene.  
```
not_guilty(P) :- has_alibi(P).
has_alibi(P) :- was_in(P, L), \+crime_scene(L).
```
These are two separate rules, because we can add other conditions to define a person who is not guilty (for example, if the person has no motive for the crime).  
  
Once we have only one suspect left, we can deduce that this person is the **murderer**.
``` 
murderer(P) :- forall(suspect(S), S=P).
```

#### Proof system  
  
*For the moment, this second part is not being implemented in user interactions, but the prolog part is working.*  

Once the user has found the murderer, they need to find evidence against them in order to be able to prosecute.  
We define two types of proof:
- An object lost by a person at the scene of the crime.
- A cigarette butt at the crime scene if the person smokes and not the victim.  
```
is_proof_against(O, P) :- lost(P, O), found_in(O, L), crime_scene(L).
is_proof_against(M, P) :- cigarette_butt(M), found_in(M, L), crime_scene(L), smoker(P), victim(V), \+smoker(V).
```
  
The user will have to give the proofs in a list, so we have a rule that checks if all the elements are proofs against the murderer.
```
list_of_proofs([]).
list_of_proofs([H|L]) :- is_proof_against(H, P), murderer(P), list_of_proofs(L).
```
  
As we saw in the section on case definition, we can define a minimum number of proofs that the user must find.
Therefore, we have a function that checks the number of proofs in the list.
```
number_of_proofs([], 0).
number_of_proofs([_|L], Z) :- list_of_proofs(L), number_of_proofs(L, Y), Z #= Y+1.
```
  
Finaly if we have a list of proofs that have the required amount of proofs the murderer can be prosecuted.
```
can_be_prosecuted(P, L) :- murderer(P), list_of_proofs(L), number_of_proofs(L,N), required_nbr_proofs(N).
```
  
### How to implement new scenarios  

We created a global template, file *fact_database.pl*, with two distinct parts, corresponding to the previous sections, Case definition and Logical rules.  
  
To implement new scenarios, simply create a new prolog document by copying the template.
The various elements of the scenario need to be implemented using the predicates presented in the **Case definition** sub-section of the previous section.  
Only the corresponding Case Definition part of the file needs to be modified, the logical rules remain unchanged whatever the content of the scenario.  

To link those new scenarios to the python game (in file *main.py*):
```
if scenario == 1:
	prolog.consult("scenario1/scenario1.pl")
elif scenario == 2:
	prolog.consult("scenario2/scenario2.pl")
elif scenario == 3:
    # add path to your 3rd scenario
else:
	prolog.consult("scenario0_exemple/exemple.pl") # this is the scenario 0
```

## User interaction  
### intro_scenario()  

This function is used to provide the player with the introductory details of the detective case they are about to play. It queries the Prolog database to get information about the characters (persons), the victim, the locations, and the crime scene, and then presents this information to the player.

### tutorial_pretended_location()  

This function is part of a tutorial guiding the player on how to interact with the Prolog back-end to get information about the suspects. Specifically, it shows how to inquire where each suspect claimed to be at the time of the murder. The user can choose to execute this query, and the results are printed out for them to read.

### find_objects_in_location(location)  

This function is used to investigate a specific location. It queries the Prolog database to find objects in the location and provides details about these objects, such as their owner, color, and additional details if available. If no objects are found, it informs the user that the location is empty.

### find_clues_for_suspects()  

This function is used to find clues related to each suspect. It queries the Prolog database to retrieve a list of suspects, then for each suspect it queries for objects that the suspect has lost. These lost objects serve as clues. The function then determines where each object was found, and presents this information to the user. If no objects/clues related to a suspect are found, it notifies the user.

### find_clues_for_uncleared_()  

This function is very similar to the find_clues_for_suspects() function. It focuses on objects each suspect owns, regardless of whether these objects are lost or not. It queries the Prolog database to find objects each suspect owns, and determines the color of these objects and whether they're lost. This information is then presented to the user. If a suspect doesn't own any objects, the user is informed.

### get_testimonies()  

This function retrieves and presents various pieces of testimony for each suspect, including who they saw, whether they're a smoker, and what objects they've lost. It queries the Prolog database to get this information and presents it to the user. 

### execute_queries(queries_done, suspects)  

This function presents the user with a list of functions that they can execute, and prompts them to choose one. If the user's choice is valid (i.e., it's a number that corresponds to one of the available functions), the chosen function is executed. The function also keeps track of the queries that have been executed (queries_done) and the current list of suspects, updating these as necessary based on the chosen function's effects. The updated lists are then returned.

### filter(queries_done, t)  

This function filters the list of executed queries based on a provided string (t). It iterates through all executed queries and checks if each query starts with the provided string. If it does, the query is added to a new list of filtered queries. This list is then returned. This function is useful for looking at subsets of executed queries that match certain criteria.

### list_(T, queries_done)  

This function is a large switch statement that handles different types of queries. For each type of query, it lists the relevant entities (objects, locations, or queries), prompts the user to choose one, and then returns the chosen entity. This function is useful for gathering user input in a structured way, depending on the type of query that's being handled.

### replace_variables(s1, s2)  

this function gets:  
s1 = "was_in(P, L) :- lost(P, O), found_in(O, L)"  
s2 = "was_in(P, L) :- lost(x_person, y_object), found_in(y_object, z_location)"  
returns --> "was_in(x_person, z_location)"  
  
### execute_prolog_command(com, queries_done, suspects, mode=None)  

This function is an essential part of your Prolog-based game application, as it enables executing Prolog commands, managing the history of executed queries and managing the suspects list.

### build_query(query, text, type, queries_done)  

This function interacts with the user to build a Prolog query. It replaces the placeholders in the query with the entities selected by the user. The list_ function is called to display a list of entities to the user and capture their selection.

### execute_queries_(chosen_function, queries_done, suspects)  

This function, execute_queries_, is designed to handle the execution of various types of Prolog queries, depending on the user's choice from a list of available queries (which are represented by the variable chosen_function).

The function primarily performs these main tasks:  
  
(1)Based on the user's choice, it prepares a Prolog query with placeholders, represented by characters such as 'P', 'O', 'L', etc., standing for Person, Object, and Location, respectively. It then calls the build_query function to interactively fill these placeholders with user-chosen real values. The build_query function presents available options to the user and asks them to make a choice, replacing the placeholders in the query with the chosen values.  
--> more complex queries are also treated as placeholders: "was_in(P, L) :- lost(P, O), found_in(O, L)" has 2 placeholders ["lost(P, O)","found_in(O, L)"] not 4 ["P","O","O","L"], this the key aspect that blocks players from skipping queries.  

(2)Once the query is prepared, depending on the complexity of the query, it either directly executes the query or performs further processing. In the case of simple queries (mode 1), it directly executes them. However, for more complex queries (mode 2 and 3), which contain sub-queries, it further replaces the placeholders in those sub-queries and then executes them. (with function replace_variables(s1, s2))  
  
(3)When executing a query, it calls the execute_prolog_command function. This function attempts to solve the given Prolog query. If it is solvable, it prints "TRUE" and adds the query to the list of executed queries (queries_done). If the query is not solvable, it prints "FALSE". If the query was a type that checks if a person has an alibi (mode 3), the person is removed from the list of suspects if the query is true.  
  
(4)After executing the query, it returns the updated list of executed queries and the updated list of suspects.  
  
## Future work  

### Fact database

The first thing to improve in the facts database would be to finish linking the *could_be_in* predicate (in the *tests/facts_test* file) to the *was_in* predicate. Another improvement would be to distinguish between objects used to describe people via *wears* and those found during the investigation. In this way, objects used solely to describe people would not appear when requesting a list of all the clues (objects).  
   
We could also imagine new features such as allowing the user to search for the murder weapon, or adding motives for the crime to certain people.  

### User interaction

The first thing to do next is to handle errors, at the moment if you choose not possible inputs as a user, you just crash everything.   
Secondly we would need to create the proof system in the user interaction side. Right now you can only alibi suspects.  
Add more more descriptions of environment/deliver clues in a more natural way.  

### Random generation of scenarios

Throughout the project, we were careful to make the implementation (for both the fact database and the interaction with the user) as independent as possible from the content of the scenarios. In fact, this makes it possible to take over the project to try and do random scenario generation, an aspect that we find very interesting but which we haven't been able to explore due to lack of time.
